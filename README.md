# DataParser

##### Api configuration:  
Example: `http://localhost:8080/data-parser/word/Adi%20Guy%20CV%202019.docx/?cvId=1&candidateId=1`  
To use it you need to specify parameters in `application.properties`:  
  1. `dropbox.base-file-path` - Path to your dropbox destination folder (/ as root).  
  2. `dropbox.accessToken` - Also you need to specify you dropbox access token.  
  3. Configure database connection.  
[How to generate access token](http://99rabbits.com/get-dropbox-access-token/)