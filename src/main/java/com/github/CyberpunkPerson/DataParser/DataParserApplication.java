package com.github.CyberpunkPerson.DataParser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@SpringBootApplication
public class DataParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataParserApplication.class, args);
    }
}
