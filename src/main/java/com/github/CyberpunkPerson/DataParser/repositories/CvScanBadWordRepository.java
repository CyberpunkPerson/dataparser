package com.github.CyberpunkPerson.DataParser.repositories;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanBadWord;
import org.springframework.data.jpa.repository.JpaRepository;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
public interface CvScanBadWordRepository extends JpaRepository<CvScanBadWord, Long> {
}
