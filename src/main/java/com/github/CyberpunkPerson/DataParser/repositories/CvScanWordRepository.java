package com.github.CyberpunkPerson.DataParser.repositories;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
public interface CvScanWordRepository extends JpaRepository<CvScanWord, Long> {

    @Modifying
    @Query(value = "insert into cv_scan_word_cv (cv_id, word_id, candidate_id) values (:cvId, :wordId, :candidateId)", nativeQuery = true)
    @Transactional
    void insertIntoCvScanWordCV(@Param("cvId") Long cvId,
                                @Param("wordId") Long wordId,
                                @Param("candidateId") Long candidateId);
}
