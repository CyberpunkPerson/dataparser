package com.github.CyberpunkPerson.DataParser.implementation;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanBadWord;
import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanWord;
import com.github.CyberpunkPerson.DataParser.repositories.CvScanWordRepository;
import com.github.CyberpunkPerson.DataParser.services.CvScanBadWordService;
import com.github.CyberpunkPerson.DataParser.services.CvScanWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@Service
public class CvScanWordServiceImpl implements CvScanWordService {

    private final CvScanWordRepository cvScanWordRepository;

    private final CvScanBadWordService cvScanBadWordService;

    @Autowired
    public CvScanWordServiceImpl(CvScanWordRepository cvScanWordRepository, CvScanBadWordService cvScanBadWordService) {
        this.cvScanWordRepository = cvScanWordRepository;
        this.cvScanBadWordService = cvScanBadWordService;
    }

    @Override
    public void deleteWord(CvScanWord cvScanWord) {
        cvScanWordRepository.delete(cvScanWord);
    }

    @Override
    public List<CvScanWord> findAll() {
        return cvScanWordRepository.findAll();
    }

    @Override
    public CvScanWord findWordById(Long id) {
        return cvScanWordRepository.findById(id).orElseThrow(NullPointerException::new);
    }

    @Override
    @Transactional
    public List<CvScanWord> saveAll(List<CvScanWord> cvScanWords, Long cvId, Long candidateId) {
        List<CvScanWord> savedCvScanWords = cvScanWordRepository.findAll();
        List<CvScanWord> finalSavedCvScanWords = savedCvScanWords;

        List<CvScanWord> uniqueWordsToSave = cvScanWords.stream()
                .filter(w -> !isContains(finalSavedCvScanWords, w))
                .collect(Collectors.toList());

        savedCvScanWords = savedCvScanWords.stream()
                .filter(w -> isContains(cvScanWords, w))
                .collect(Collectors.toList());

        uniqueWordsToSave = cvScanWordRepository.saveAll(uniqueWordsToSave);

        uniqueWordsToSave.forEach(w -> cvScanWordRepository.insertIntoCvScanWordCV(cvId,
                w.getWordId(),
                candidateId));

        savedCvScanWords.forEach(w -> cvScanWordRepository.insertIntoCvScanWordCV(cvId,
                w.getWordId(),
                null));

        savedCvScanWords.addAll(uniqueWordsToSave);
        return savedCvScanWords;
    }

    private boolean isContains(final List<CvScanWord> wordsList, final CvScanWord checkWord) {
        return wordsList.stream().anyMatch(w -> w.getName().equals(checkWord.getName()));
    }

    @Override
    public List<CvScanWord> extractWords(List<String> wordsList) {
        List<String> wordsToSave = filterWords(wordsList);
        List<CvScanWord> cvScanWords = new ArrayList<>();

        for (String word : wordsToSave) {
            CvScanWord cvScanWord = new CvScanWord();
            cvScanWord.setName(word);
            cvScanWords.add(cvScanWord);
        }

        return cvScanWords;
    }

    @Override
    public List<String> filterWords(List<String> wordsList) {
        List<CvScanBadWord> savedCvScanBadWords = cvScanBadWordService.findAll();

        List<String> deprecatedWords = savedCvScanBadWords.stream()
                .map(CvScanBadWord::getName)
                .collect(Collectors.toList());

        return wordsList.stream()
                .filter(w -> !deprecatedWords.contains(w))
                .distinct()
                .collect(Collectors.toList());
    }
}
