package com.github.CyberpunkPerson.DataParser.implementation;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanBadWord;
import com.github.CyberpunkPerson.DataParser.repositories.CvScanBadWordRepository;
import com.github.CyberpunkPerson.DataParser.services.CvScanBadWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@Service
public class CvScanBadWordServiceImpl implements CvScanBadWordService {

    private final CvScanBadWordRepository cvScanBadWordRepository;

    @Autowired
    public CvScanBadWordServiceImpl(CvScanBadWordRepository cvScanBadWordRepository) {
        this.cvScanBadWordRepository = cvScanBadWordRepository;
    }

    @Override
    public void deleteBadWord(CvScanBadWord cvScanBadWord) {
        cvScanBadWordRepository.delete(cvScanBadWord);
    }

    @Override
    public List<CvScanBadWord> findAll() {
        return cvScanBadWordRepository.findAll();
    }

    @Override
    public CvScanBadWord findBadWordById(Long id) {
        return cvScanBadWordRepository.findById(id).orElseThrow(NullPointerException::new);
    }

    @Override
    public List<CvScanBadWord> saveAll(List<CvScanBadWord> cvScanBadWords) {
        return cvScanBadWordRepository.saveAll(cvScanBadWords);
    }
}
