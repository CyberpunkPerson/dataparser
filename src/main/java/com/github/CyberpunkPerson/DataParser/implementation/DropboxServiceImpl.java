package com.github.CyberpunkPerson.DataParser.implementation;

import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.*;
import com.github.CyberpunkPerson.DataParser.exceptions.DropboxException;
import com.github.CyberpunkPerson.DataParser.services.DropboxActionResolver;
import com.github.CyberpunkPerson.DataParser.services.DropboxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 02.12.18
 **/
@Service
public class DropboxServiceImpl implements DropboxService {

    private final DbxClientV2 dbxClient;

    @Autowired
    public DropboxServiceImpl(DbxClientV2 dbxClient) {
        this.dbxClient = dbxClient;
    }

    @Override
    public InputStream downloadFile(String filePath) {
        return handleDropboxAction(() -> dbxClient.files().download(filePath).getInputStream(),
                String.format("Error downloading file: $s", filePath));
    }

    @Override
    public FileMetadata uploadFile(String filePath, InputStream fileStream) {
        return handleDropboxAction(() -> dbxClient.files().uploadBuilder(filePath).uploadAndFinish(fileStream),
                String.format("Error uploading file: %s", filePath));
    }

    @Override
    public CreateFolderResult createFolder(String folderPath) {
        return handleDropboxAction(() -> dbxClient.files().createFolderV2(folderPath), "Error creating folder");
    }

    @Override
    public FolderMetadata getFolderDetails(String folderPath) {
        return getMetadata(folderPath, FolderMetadata.class, String.format("Error getting folder details: %s", folderPath));
    }

    @Override
    public FileMetadata getFileDetails(String filePath) {
        return getMetadata(filePath, FileMetadata.class, String.format("Error getting file details: %s", filePath));
    }

    @Override
    public ListFolderResult listFolder(String folderPath, boolean recursiveListing, long limit) {
        ListFolderBuilder listFolderBuilder = dbxClient.files().listFolderBuilder(folderPath);
        listFolderBuilder.withRecursive(recursiveListing);
        listFolderBuilder.withLimit(limit);

        return handleDropboxAction(listFolderBuilder::start, String.format("Error listing folder: %s", folderPath));
    }

    @Override
    public ListFolderResult listFolderContinue(String cursor) {
        return handleDropboxAction(() -> dbxClient.files().listFolderContinue(cursor), "Error listing folder");
    }

    @Override
    public void deleteFile(String filePath) {
        handleDropboxAction(() -> dbxClient.files().deleteV2(filePath), String.format("Error deleting file: %s", filePath));
    }

    @Override
    public void deleteFolder(String folderPath) {
        handleDropboxAction(() -> dbxClient.files().deleteV2(folderPath), String.format("Error deleting folder: %s", folderPath));
    }

    private <T> T handleDropboxAction(DropboxActionResolver<T> action, String exceptionMessage) {
        try {
            return action.perform();
        } catch (Exception e) {
            String messageWithCause = String.format("%s with cause: %s", exceptionMessage, e.getMessage());
            throw new DropboxException(messageWithCause, e);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T getMetadata(String path, Class<T> type, String message) {
        Metadata metadata = handleDropboxAction(() -> dbxClient.files().getMetadata(path),
                String.format("Error accessing details of: %s", path));

        checkIfMetadataIsInstanceOfGivenType(metadata, type, message);
        return (T) metadata;
    }

    private <T> void checkIfMetadataIsInstanceOfGivenType(Metadata metadata, Class<T> validType, String exceptionMessage) {
        boolean isValidType = validType.isInstance(metadata);
        if (!isValidType) {
            throw new DropboxException(exceptionMessage);
        }
    }
}
