package com.github.CyberpunkPerson.DataParser.implementation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 03.12.18
 **/
public abstract class DocumentParser {

    private static Pattern pHebrewWords = Pattern.compile("[\\p{InHebrew}]+");
    private static Pattern pLatinWords = Pattern.compile("[\\p{IsLatin}]+");

    abstract String parse(InputStream inputStream) throws IOException;

    public List<String> findAllWordsInString(String inputString) {
        List<String> wordsList = new ArrayList<>();

        String[] arrayOfWords = inputString.split("\\s+");

        for (String arrayOfWord : arrayOfWords) {
            if (pHebrewWords.matcher(arrayOfWord).matches() || pLatinWords.matcher(arrayOfWord).matches()) {
                wordsList.add(arrayOfWord);
            }
        }

        return wordsList;
    }

    public List<String> findAllWordsInStream(InputStream inputStream) throws IOException {
        return findAllWordsInString(parse(inputStream));
    }
}
