package com.github.CyberpunkPerson.DataParser.implementation;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 03.12.18
 **/
@Service
public class WordParser extends DocumentParser {

    @Override
    public String parse(InputStream inputStream) throws IOException {
        try (XWPFDocument xwpfDocument = new XWPFDocument(inputStream)) {
            XWPFWordExtractor extractor = new XWPFWordExtractor(xwpfDocument);
            return extractor.getText();
        }
    }
}
