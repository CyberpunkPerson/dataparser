package com.github.CyberpunkPerson.DataParser.implementation;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 03.12.18
 **/
@Service
public class PDFParser extends DocumentParser {

    @Override
    public String parse(InputStream inputStream) throws IOException {

        try (PDDocument document = PDDocument.load(inputStream)) {

            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            return pdfTextStripper.getText(document);
        }
    }
}
