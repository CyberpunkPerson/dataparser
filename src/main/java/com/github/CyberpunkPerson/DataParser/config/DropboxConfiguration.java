package com.github.CyberpunkPerson.DataParser.config;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 02.12.18
 **/
@Configuration
public class DropboxConfiguration {

    @Value("${dropbox.accessToken}")
    private String accessToken;

    @Bean
    public DbxClientV2 dropboxClient() {
        DbxRequestConfig dbxRequestConfig = DbxRequestConfig.newBuilder("DataParser").build();
        return new DbxClientV2(dbxRequestConfig, accessToken);
    }
}
