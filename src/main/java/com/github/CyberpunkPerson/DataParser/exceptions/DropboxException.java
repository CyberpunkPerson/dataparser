package com.github.CyberpunkPerson.DataParser.exceptions;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 02.12.18
 **/
public class DropboxException extends RuntimeException {

    public DropboxException(String message) {
        super(message);
    }

    public DropboxException(String message, Exception cause) {
        super(message, cause);
    }
}
