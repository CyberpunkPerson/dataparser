package com.github.CyberpunkPerson.DataParser.services;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 02.12.18
 **/
@FunctionalInterface
public interface DropboxActionResolver<T> {
    T perform() throws Exception;
}
