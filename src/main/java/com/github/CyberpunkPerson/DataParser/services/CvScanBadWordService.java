package com.github.CyberpunkPerson.DataParser.services;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanBadWord;

import java.util.List;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
public interface CvScanBadWordService {

    void deleteBadWord(CvScanBadWord cvScanBadWord);

    List<CvScanBadWord> findAll();

    CvScanBadWord findBadWordById(Long id);

    List<CvScanBadWord> saveAll(List<CvScanBadWord> cvScanBadWords);
}
