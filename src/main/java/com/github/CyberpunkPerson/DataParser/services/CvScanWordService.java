package com.github.CyberpunkPerson.DataParser.services;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanWord;

import java.util.List;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
public interface CvScanWordService {

    void deleteWord(CvScanWord cvScanWord);

    List<CvScanWord> findAll();

    CvScanWord findWordById(Long id);

    List<CvScanWord> saveAll(List<CvScanWord> cvScanWords, Long cvId, Long candidateId);

    List<CvScanWord> extractWords(List<String> wordsList);

    List filterWords(List<String> wordsList);
}
