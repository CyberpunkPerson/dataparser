package com.github.CyberpunkPerson.DataParser.controllers;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanBadWord;
import com.github.CyberpunkPerson.DataParser.services.CvScanBadWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@RestController
@RequestMapping("/CvScanBadWords")
public class CvScanBadWordController {

    private final CvScanBadWordService cvScanBadWordService;

    @Autowired
    public CvScanBadWordController(CvScanBadWordService cvScanBadWordService) {
        this.cvScanBadWordService = cvScanBadWordService;
    }

    @RequestMapping("/findAll")
    List<CvScanBadWord> findAll() {
        return cvScanBadWordService.findAll();
    }

    @RequestMapping(value = "/saveAll", method = RequestMethod.POST)
    public List<CvScanBadWord> saveBadWords(@RequestBody List<CvScanBadWord> cvScanWords) {
        return cvScanBadWordService.saveAll(cvScanWords);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void deleteBadWord(@RequestBody CvScanBadWord cvScanBadWord) {
        cvScanBadWordService.deleteBadWord(cvScanBadWord);
    }

    @RequestMapping(value = "/{wordId}", method = RequestMethod.GET)
    public CvScanBadWord findBadWordById(@PathVariable Long wordId) {
        return cvScanBadWordService.findBadWordById(wordId);
    }
}
