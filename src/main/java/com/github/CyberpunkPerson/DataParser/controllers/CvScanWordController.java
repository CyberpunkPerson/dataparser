package com.github.CyberpunkPerson.DataParser.controllers;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanWord;
import com.github.CyberpunkPerson.DataParser.services.CvScanWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@RestController
@RequestMapping("/CvScanWords")
public class CvScanWordController {

    private final CvScanWordService cvScanWordService;

    @Autowired
    public CvScanWordController(CvScanWordService cvScanWordService) {
        this.cvScanWordService = cvScanWordService;
    }

    @RequestMapping(value = "/findAll")
    public List<CvScanWord> findAll() {
        return cvScanWordService.findAll();
    }

    @RequestMapping(value = "/saveAll/{cvId}/{candidateId}", method = RequestMethod.POST)
    public List<CvScanWord> saveWords(@RequestBody List<CvScanWord> cvScanWords,
                                      @PathVariable Long cvId,
                                      @PathVariable Long candidateId) {

        return cvScanWordService.saveAll(cvScanWords, cvId, candidateId);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void deleteWord(@RequestBody CvScanWord cvScanWord) {
        cvScanWordService.deleteWord(cvScanWord);
    }

    @RequestMapping(value = "/{wordId}", method = RequestMethod.GET)
    public CvScanWord findWordById(@PathVariable Long wordId) {
        return cvScanWordService.findWordById(wordId);
    }
}
