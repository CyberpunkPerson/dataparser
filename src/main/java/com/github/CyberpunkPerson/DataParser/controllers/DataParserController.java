package com.github.CyberpunkPerson.DataParser.controllers;

import com.github.CyberpunkPerson.DataParser.domain.entities.CvScanWord;
import com.github.CyberpunkPerson.DataParser.implementation.PDFParser;
import com.github.CyberpunkPerson.DataParser.implementation.WordParser;
import com.github.CyberpunkPerson.DataParser.services.CvScanWordService;
import com.github.CyberpunkPerson.DataParser.services.DropboxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@Controller
@RequestMapping("/data-parser")
public class DataParserController {

    private final DropboxService dropboxService;

    private final CvScanWordService cvScanWordService;

    private final PDFParser pdfParser;

    private final WordParser wordParser;

    @Value("${dropbox.base-file-path}")
    private String BASE_FILE_PATH;

    @Autowired
    public DataParserController(DropboxService dropboxService, CvScanWordService cvScanWordService, PDFParser pdfParser, WordParser wordParser) {
        this.dropboxService = dropboxService;
        this.cvScanWordService = cvScanWordService;
        this.pdfParser = pdfParser;
        this.wordParser = wordParser;
    }


    @GetMapping(value = "/pdf/{filePath}/",
            params = {"cvId", "candidateId"})
    public void extractWordsFromPDFDocument(@PathVariable String filePath,
                                            @PathVariable Long cvId,
                                            @PathVariable Long candidateId) throws IOException {
        filePath = BASE_FILE_PATH + filePath;
        InputStream inputStream = dropboxService.downloadFile(filePath);

        List<String> parsedWords = pdfParser.findAllWordsInStream(inputStream);

        List<CvScanWord> cvScanWordsToSave = cvScanWordService.extractWords(parsedWords);

        cvScanWordService.saveAll(cvScanWordsToSave, cvId, candidateId);
    }

    @GetMapping(value = "/word/{filePath}/",
            params = {"cvId", "candidateId"})
    public void extractWordsFromWordDocument(@PathVariable String filePath,
                                             @RequestParam Long cvId,
                                             @RequestParam Long candidateId) throws IOException {
        filePath = BASE_FILE_PATH + filePath;
        InputStream inputStream = dropboxService.downloadFile(filePath);

        List<String> parsedWords = wordParser.findAllWordsInStream(inputStream);

        List<CvScanWord> cvScanWordsToSave = cvScanWordService.extractWords(parsedWords);

        cvScanWordService.saveAll(cvScanWordsToSave, cvId, candidateId);
    }

}
