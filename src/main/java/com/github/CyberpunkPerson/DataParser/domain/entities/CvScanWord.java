package com.github.CyberpunkPerson.DataParser.domain.entities;

import lombok.Data;

import javax.persistence.*;

/*
 * @project: DataParser
 * @author Andrew V. Belunov on 01.12.18
 **/
@Data
@Entity
@Table(name = "cv_scan_words")
public class CvScanWord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "word_id")
    private Long wordId;

    @Column(name = "name")
    private String name;
}
