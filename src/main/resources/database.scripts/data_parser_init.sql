create table data_parser.cv_scan_words (
  word_id   bigint primary key auto_increment,
  name varchar(100) character set utf8 collate utf8_bin not null,
  unique index word_name (name)
);

create table data_parser.cv_scan_bad_words (
  word_id   bigint primary key auto_increment,
  name varchar(100) character set utf8 collate utf8_bin not null,
  unique index bad_word_name (name)
);

create table data_parser.cv_scan_word_cv (
  cv_id         bigint not null,
  word_id       bigint not null,
  candidate_id  bigint,
  foreign key (word_id)
      references data_parser.cv_scan_words (word_id)
      on delete cascade,
  index cv_word (cv_id, word_id)
);